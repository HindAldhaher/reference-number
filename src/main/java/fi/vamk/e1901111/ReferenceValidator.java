package fi.vamk.e1901111;



import java.security.InvalidAlgorithmParameterException;

import java.util.ArrayList;
import java.util.Objects;
import java.util.List;

public final class ReferenceValidator 
{
    private ReferenceValidator() {
    }
    public static List<String> generate(String base, int amount) throws InvalidAlgorithmParameterException {

        validateBaseFormat(base);

        long baseDigit = Long.parseLong(base);

        List<String> referenceNumbers = new ArrayList<String>();

        for (int i = 0; i < amount; i++) {
            referenceNumbers.add(String.valueOf(baseDigit) + calculateCheckSum(Long.toString(baseDigit)));
            baseDigit++;
        }

        return referenceNumbers;
    }

    private static int calculateCheckSum(String base) {

        Integer[] multipliers = new Integer[] { 7, 3, 1 };
        int multiplierIndex = 0;
        int sum = 0;

        // multiplication from right to left
        for (int i = base.length() - 1; i >= 0; i--) {

            if (multiplierIndex > 2) {
                multiplierIndex = 0;
            }

            int value = Character.getNumericValue(base.charAt(i));
            sum += value * multipliers[multiplierIndex];
            multiplierIndex++;
        }

        return calculateCheckDigit(sum);
    }

    private static int calculateCheckDigit(int sum) {

        int checkDigit = 10 - sum % 10;

        if (checkDigit == 10)
        {
            checkDigit = 0;
        }

        return checkDigit;
    }

    private static void validateBaseFormat(String base) throws InvalidAlgorithmParameterException {
        if (Objects.isNull(base) || base.trim().isEmpty()) {
            throw new InvalidAlgorithmParameterException("base length MUST be between 3 and 20 characters");
        }

        if (base.length() < 3 || base.length() > 20) {
            throw new InvalidAlgorithmParameterException("base length MUST be between 3 and 20 characters");
        }
    }
}
